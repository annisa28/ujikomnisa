 <!DOCTYPE html>
<?php
include "../koneksi.php";
include "header_admn.php";
?>
<html>
<head>
  <title>INVENSKANIC</title>
  <link rel="stylesheet" type="../text/css" href="../css/bootstrap.min.css">
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../data_table/assets/css/jquery.dataTables.css">
  <div class="panel panel-default">
</head>
<body><div class="container">
<br>
  <div  style="white-space: nowrap; font-size: 24px ">DATA Barang<span style="white-space: nowrap; font-size: 15px"> SMKN 1 Ciomas</span></div>
  <div class="kategori" style="padding-left: 20px;">
<?php   
    $sql=mysqli_query($koneksi,"SELECT * FROM inventaris GROUP BY sarana");
    while($data1=mysqli_fetch_array($sql)){
 
  echo"<a href='peminjaman_b.php?sarana=$data1[sarana]' class='btn btn-primary' style='margin-left: 10px'>$data1[sarana]</a>";
  
    }
  ?>
</div>
   <br>
  <div class="panel panel-default">
  <!-- Default panel contents -->
  
  <!-- Table -->
  <table class="table" id="example">
    <thead>
      <tr class="info">
      <th>no</th>
      <th>nama barang</th>
      <th>kondisi</th>
      <th>jumlah</th>
      <th>jenis</th>
      <th>tanggal register</th>
      <th>ruang</th>
      <th>kode inventaris</th>
      <th>aksi</th>
    </tr>
    </thead>

    <tbody>
      <?php
      include"../koneksi.php";
      
      if(isset($_GET['sarana'])) {
      $pilih=mysqli_query($koneksi,"SELECT * from inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang nisa ON i.id_ruang=nisa.id_ruang WHERE i.sarana='$_GET[sarana]'");  
      }else{
      $pilih=mysqli_query($koneksi,"SELECT * from inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang nisa ON i.id_ruang=nisa.id_ruang ");
      }
      $no=1;
      while($data=mysqli_fetch_array($pilih)){
        ?>
        <style type="text/css">
        th{
          text-align: center;
        }
        td{
          text-align: center;
        }
        </style>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?=$data['nama'];?></td>
          <td><?=$data['kondisi']; ?></td>
          <td><?=$data['jumlah']; ?></td>
          <td><?=$data['nama_jenis']; ?></td>
          <td><?=$data['tanggal_register']; ?></td>
          <td><?=$data['nama_ruang']; ?></td>
          <td><?=$data['kode_inventaris']; ?></td>
          
          <td>
          <a style="font-size: 20px" class="glyphicon glyphicon-check" data-toggle="modal" data-target="#myModal-check-<?php echo $data['id_inventaris'] ?>" href="#"></a>
        
        </td>
        <div class="modal fade" id="myModal-check-<?php echo $data['id_inventaris'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">detail barang</h4>
      </div>
      <div class="modal-body">
        <p align="center"><img src="skanic.png" style="width : 25%" align="center">
        </p>
        <br>nama barang: <?php echo $data['nama']; ?></br>
        <br>kondisi: <?php echo $data['kondisi']; ?></br>
        <br>keterangan: <?php echo $data['keterangan']; ?>
        <br>jumlah: <?php echo $data['jumlah']; ?></br>
        <br>jenis: <?php echo $data['nama_jenis']; ?></br>
        <br>tanggal register: <?php echo $data['tanggal_register']; ?></br>
        <br>ruang: <?php echo $data['nama_ruang']; ?></br>
        <br>kode inventaris: <?php echo $data['kode_inventaris']; ?></br>
        <br>sarana: <?php echo $data['sarana']; ?></br>
      </div>
      <div class="modal-footer">
        <a href="peminjaman.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"><button type="button" class="btn btn-primary" name="simpan" >Pinjam</button></a>
      </div>
    </div>
  </div>
</div>          

        </tr>
        <!-- Modal -->
<div class="modal fade" id="myModal-edit-<?php echo $data['id_inventaris'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">masukan data</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="POST" action="proses_pinjam.php">
 
   <div class="form-group">
    <label for="nama">nama barang</label>
    <input type="hidden" class="form-control" name="id_inventaris" value="<?php echo $data['id_inventaris']; ?>">
    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $data['nama']; ?>">
  </div>
   <div class="form-group">
    <label for="kondisi">kondisi</label>
    <input type="text" class="form-control" id="kondisi" name="kondisi" placeholder="Masukan kondisi" value="<?php echo $data['kondisi'] ?>" >
  </div>
   <div class="form-group">
    <label for="keterangan">keterangan</label>
    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Masukan keterangan" value="<?php echo $data['keterangan'] ?>">
  </div>
   <div class="form-group">
    <label for="jumlah">jumlah</label>
    <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Masukan jumlah" value="<?php echo $data['jumlah'] ?>">
  </div>
   <div class="form-group">
    <label for="id_jenis">id jenis</label>
    <input type="text" class="form-control" id="id_jenis" name="id_jenis" placeholder="Masukan id" value="<?php echo $data['id_jenis'] ?>">
  </div>
  <div class="form-group">
    <label for="id_ruang">id ruang</label>
    <input type="text" class="form-control" id="id_ruang" name="id_ruang" placeholder="Masukan id" value="<?php echo $data['id_ruang'] ?>">
  </div>
  <div class="form-group">
    <label for="kode_inventaris">kode inventaris</label>
    <input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" placeholder="Masukan kode" value="<?php echo $data['kode_inventaris'] ?>">
  </div>
  <!--
<div class="form-group">
    <label for="sarana">sarana</label>
    <input type="text" class="form-control" id="sarana" name="sarana" placeholder="Masukan sarana">
  </div>
  <td><img src="<?php echo $data['gambar']; ?>" width="50" height="50"></td>
  -->
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="edit" >Submit</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>
      <?php
      $no++;
      }
      ?>
    </tbody>
  </table>
</div>

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>




















