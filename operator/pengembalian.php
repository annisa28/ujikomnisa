  <!DOCTYPE html>
<?php
include "../koneksi.php";
include "header_admn.php";
?>
<html>
<head>
  <title>INVENSKANIC</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../data_table/assets/css/jquery.dataTables.css">
  <div class="panel panel-default">
  <style type="text/css">
  li{
    list-style: none;
  }
  </style>
</head>
<body>

<div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA INVENTARIS</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Tanggal Pinjam</td>
                        <td>Jumlah</td>
                        <td>Status</td>
                        <td>Peminjam</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                     <?php
                    $no=1;
                     $pilih=mysqli_query($koneksi, "SELECT * FROM peminjaman p LEFT JOIN inventaris i ON p.id_inventaris=i.id_inventaris JOIN detail_pinjam d ON p.kode_peminjaman=d.kode_peminjaman WHERE status_peminjaman = 'pinjam' order by p.id_peminjaman desc");
                    while($data=mysqli_fetch_array($pilih)){
                        $id_pegawai = $data['id_pegawai'];
                        $q_data_pegawai = mysqli_query($koneksi, "SELECT * from petugas where id_petugas ='$id_pegawai'");
                        $data_pegawai= mysqli_fetch_array($q_data_pegawai);
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama'];?></td>
                        <td><?=$data['tanggal_pinjam'];?></td>
                        <td><?=$data['jumlah_pinjam'];?></td>
                        <td><?=$data['status_peminjaman'];?></td>
                        <td><?=$data_pegawai['nama_petugas'];?></td>
                        <td>
                            <?php echo "<a class='btn btn-success' href='proses_kembali.php?id_peminjaman=$data[id_peminjaman]&id_inventaris=$data[id_inventaris]&jumlah_pinjam=$data[jumlah_pinjam]' >Kembalikan</a> " ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>

    </tbody>
  </table>
</div>
<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
     <div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA PENGEMBALIAN</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Tanggal Kembali</td>
                        <td>Jumlah</td>
                        <td>Status</td>
                        <td>Peminjam</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    $pilih=mysqli_query($koneksi, "SELECT * FROM peminjaman JOIN detail_pinjam ON peminjaman.kode_peminjaman=detail_pinjam.kode_peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai LEFT JOIN Inventaris ON peminjaman.id_inventaris=inventaris.id_inventaris where status_peminjaman ='Dikembalikan'  order by tanggal_kembali desc");
                    while($data=mysqli_fetch_array($pilih)){
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama'];?></td>
                        <td><?=$data['tanggal_pinjam'];?></td>
                        <td><?=$data['jumlah_pinjam'];?></td>
                        <td><?=$data['status_peminjaman'];?></td>
                        <td><?=$data['nama_pegawai'];?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>