-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 02:07 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah_pinjam` varchar(50) NOT NULL,
  `kode_peminjaman` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`, `kode_peminjaman`) VALUES
(1, 10, '2', 'XORHL6Q0'),
(2, 12, '2', 'YPV5XKWC'),
(3, 11, '4', 'PFA2LNLI'),
(4, 10, '2', '5I0K4999'),
(5, 11, '9', 'QC0G2D6W'),
(6, 11, '1', '0N64CLUP'),
(7, 11, '1', 'PHFKRDQD'),
(8, 11, '1', '9IVKUM4I'),
(9, 13, '1', 'NTXVHI1C'),
(10, 11, '1', 'FTI6T7E0'),
(11, 14, '1', 'K3QPJKH6'),
(12, 18, '1', 'F6K3IPDS'),
(13, 19, '1', '2Z670LZ6'),
(14, 20, '1', '9ARDJETM'),
(15, 21, '1', 'DST9QD3R'),
(16, 22, '1', '44OKL7VZ'),
(17, 18, '1', 'D478VZ6A'),
(18, 19, '1', '7PM1GPML'),
(19, 18, '1', '0LAIX4A6'),
(20, 21, '1', '5NB7U74I'),
(21, 19, '1', 'AZA601IT'),
(22, 18, '1', '7DCWZYRL'),
(23, 18, '1', 'SVSR9YPG'),
(24, 18, '1', 'TVU4J7W1'),
(25, 18, '1', 'U141QX6I'),
(26, 18, '1', 'A0TMFV1Z'),
(27, 20, '1', '6X0RHAE3'),
(28, 22, '1', 'CIASKIJL'),
(29, 18, '1', 'MCXS96WQ'),
(30, 18, '1', 'UGEZA2W8'),
(31, 20, '1', 'M20AJY7P'),
(32, 22, '1', 'XRB1KK7R'),
(33, 18, '1', 'KMWEUJ09'),
(34, 20, '1', 'XDF5RR6F'),
(35, 18, '1', '6NWSCPAJ'),
(36, 23, '1', '0LE3CW1J'),
(37, 24, '1', '3TNM6PUE'),
(38, 23, '1', 'NM6B6MB4'),
(39, 24, '1', 'FF4CK9E1');

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(500) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(15) NOT NULL,
  `sarana` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `sarana`, `id_petugas`) VALUES
(18, 'obeng', 'baik', 'pel pro', '9', 7, '2019-04-07 18:43:11', 4, '8', 'TKR', 0),
(20, 'laptop', 'baik', 'pel pro', '7', 6, '2019-04-06 15:51:55', 4, '3', 'RPL', 0),
(22, 'kamera', 'kurang baik', 'pel pro', '8', 6, '2019-04-06 15:56:16', 4, '7', 'BROADCASTING', 0),
(23, 'wig', 'baik', 'pel pro', '9', 4, '2019-04-07 18:42:51', 5, '22', 'ANIMASI', 0),
(24, 'kacamata', 'baik', 'pel pro', '8', 4, '2019-04-07 18:44:11', 4, '11', 'TPL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(4, 'popo', '4', 'pel pro'),
(6, 'komputer', '6', 'pel pro'),
(7, 'elektro', '6', 'pel pro');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL,
  `nama_pegawai` varchar(20) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `id_level`) VALUES
(1, 'nisa', 1234, 'bogor', 3),
(2, 'wawa', 12358, 'ciomas', 2),
(3, 'nisaaaa', 12345, 'bogor', 3);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL,
  `kode_peminjaman` varchar(15) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `id_inventaris` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `kode_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`, `id_inventaris`) VALUES
(31, '6NWSCPAJ', '2019-04-07 18:46:40', '0000-00-00 00:00:00', 'Pinjam', 1, 18),
(32, '0LE3CW1J', '2019-04-07 18:47:53', '2019-04-07 14:01:21', 'Dikembalikan', 12358, 23),
(33, '3TNM6PUE', '2019-04-07 18:49:04', '0000-00-00 00:00:00', 'Pinjam', 1, 24),
(34, 'NM6B6MB4', '2019-04-07 19:00:58', '0000-00-00 00:00:00', 'Pinjam', 12358, 23),
(35, 'FF4CK9E1', '2019-04-07 19:06:06', '0000-00-00 00:00:00', 'Pinjam', 1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(20) NOT NULL,
  `id_level` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`) VALUES
(12357, 'anisa', '123456', 'anisa', 1),
(12358, 'wawa', '1234', 'wawa', 2),
(12359, 'unang', 'unang', 'nisa', 3),
(12360, 'unang', 'nisa', 'unang', 1),
(12361, 'wiwi', 'wiwi', 'wiwi', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL,
  `nama_ruang` varchar(20) NOT NULL,
  `kode_ruang` int(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(4, 'lab2', 2, 'pel pro'),
(5, 'lab animasi', 3, 'pel pro'),
(6, 'lab1', 1, 'pel pro');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12362;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
