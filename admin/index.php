<?php
include 'header_admn.php';
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title> Proyek 1</title>
</head>
<body>
	<div class="row">
		<div class="col-md-8">
			<p>Inventarisasi yang berarti daftar barang-barang, berasal dari kata “inventaris” (Latin = inventarium). Inventarisasi sarana dan prasarana sekolah merupakan pencatatan barang-barang milik sekolah ke dalam suatu daftar inventaris yang tertib dan teratur sesuai dengan tata cara yang berlaku. Barang inventaris sekolah adalah semua barang milik negara, memiliki ciri-ciri barang ekonom yang dikuasai sekolah melalui dana dari pemerintah, DPP, pertukaran, hadiah, hibah ataupun hasil usaha pembuatan sendiri untuk menunjang jalannya proses belajar mengajar. Kepala sekolah yang bertanggung jawab pada inventarisasi fisik dan pengisian daftar inventaris barang sekolah. Tujuan adanya inventarisasi sarana dan prasarana pendidikan adalah :<p>
				1. Menjaga dan menciptakan administrasi sarana dan prasarana yang dimiliki sekolah.<p>
				2. Menekan pengeluaran sekolah baik dalam pengadaan ataupun pemeliharaan serta penghapusan sarana dan prasarana sekolah.<p>
				3. Pedoman untuk menghitung kekayaan sekolah dalam bentuk laporan inventaris<p>
				4. Memudahkan pengawasan dan pengendalian terhadap barang inventaris sekolah

			</p>
		</div>
		<div class="col-md-4">
			<img src="inven.jpg">
		</div>
	</div>
</body>
</html>

