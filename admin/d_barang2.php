 <!DOCTYPE html>
<?php
include 'koneksi.php';
$sarana=$_GET['sarana'];
$pilih =mysqli_query($koneksi,"SELECT * FROM inventaris WHERE sarana='$sarana'");
  $data =mysqli_fetch_array($pilih);
?>
<html>
<head>
	<title>INVENSKANIC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link href="css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="data_table/assets/css/jquery.dataTables.css">
	<div class="panel panel-default">
  <style type="text/css">
  th{
    background-color: teal;
    color: white;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-default" >
<div class="panel-footer" style="background-color: teal;color: white;"> <img src="skanic.png" style="width : 3%">   INVENSKANIC
 <span style="float: right;color: white"><a href="logout.php"><i class="glyphicon glyphicon-log-out" style="color: white;font-size: larger;margin-top: 9px;"></i></a></span></div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
          </ul>
       
      
     <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_admin.php">Admin</a></li>
    <li><a href="d_operator.php">Operator</a></li>
    <li><a href="d_user.php">User</a></li>
  </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php"> Ruang</a></li>
    <li><a href="d_jenis.php"> Jenis</a></li>

  </ul>
          <li><a href="peminjaman_b.php"><span class="glyphicon glyphicon-resize-full"></span> Peminjam</a></li>
          <li><a href="pengembalian.php"><span class="glyphicon glyphicon-resize-small"></span> Pengembalian</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-print"></span> Laporan<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="laporan_barang.php">Data Barang</a></li>
    <li><a href="laporan_peminjaman2.php">Data peminjaman</a></li>
    <li><a href="laporan_pengembalian.php">Data Pengembalian</a></li>
          
          
        </li>
        </li>
       
      </ul>
          <li><a href="backup_database.php"><span class="glyphicon glyphicon-bookmark"></span> BackupDatabase</a></li>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php 
?>

<div class="container">
	<div  style="white-space: nowrap; font-size: 24px ">Jurusan<span style="white-space: nowrap; font-size: 15px"> SMKN 1 Ciomas</span></div>
   <br>
  <div class="panel panel-default">
  <!-- Default panel contents -->
  
  <div class="container">
  <div class="row">
  <?php 

    $sql = mysqli_query($koneksi,"SELECT * FROM inventaris i LEFT JOIN jenis j ON i.id_jenis=j.id_jenis LEFT JOIN ruang r ON i.id_ruang=r.id_ruang WHERE sarana='$sarana' ");
    while($data=mysqli_fetch_array($sql)){
   ?>
  <table class="table table-striped" border="1">
  <tr>
    <td>No</td>
    <td>merk</td>
    <td>gambar</td>
    <td>keterangan</td>
    <td>Aksi</td>
  </tr>

      <tr>
        <td><?=$no++; ?></td>
        <td><?=$data['merk']; ?></td>
        <td><img src="<?php echo $data['gambar']; ?>" width="50" height="50"></td>
        <td><?php if($data['keterangan']==1){echo "Tersedia";}else{echo "Dipinjam";} ; ?></td>
          
        <td>
        <p><a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal-<?php echo $data['id_inventaris']; ?>">detail</a>
      </div>
    </div>
  </div>
  <div class="modal fade" id="myModal-<?php echo $data['id_inventaris'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">DATA</h4>
      </div>
      <div class="modal-body">
      <h2><?php echo $data['nama'] ?></h2>
      <table>
      <tr>
        <td>Nama Barang</td>
        <td>:</td>
        <td><?php echo $data['nama'] ?></td>
      </tr>
      <tr>
        <td>Jumlah</td>
        <td>:</td>
        <td><?php echo $data['jumlah'] ?></td>
      </tr>
      <tr>
        <td>Kondisi</td>
        <td>:</td>
        <td><?php echo $data['kondisi'] ?></td>
      </tr>
      <tr>
        <td>Jenis</td>
        <td>:</td>
        <td><?php echo $data['nama_jenis'] ?></td>
      </tr>
      <tr>
        <td>Ruang</td>
        <td>:</td>
        <td><?php echo $data['nama_ruang'] ?></td>
      </tr>

      </table>
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </div>
      
    </div>
  </div>
</div>
  <?php } ?>
</div>
  
</div>
</div>
 
</div>

</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>