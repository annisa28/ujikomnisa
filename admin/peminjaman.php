<?php
include "../koneksi.php";
include "header_admn.php";
include "kd_pinjam.php";

?>

<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>Form Peminjam</center></b></div>
        <div class="panel-body">
        <div class="col-lg-12">
        <?php 
        include"../koneksi.php";
          $id_inventaris = $_GET['id_inventaris'];
          $sql = mysqli_query($koneksi,"SELECT * FROM inventaris where id_inventaris='$id_inventaris'");
          $data = mysqli_fetch_array($sql);
         ?>
            <form action="proses_pinjam.php" method="POST" enctype="multipart/form-data" >
                <div class="form-group">
                <div class="form-group">
                    <label>Kode peminjaman</label>
                    <input type="text" class="form-control" name="kd_pinjam" value=<?php echo $kd ?> readonly></input>
                </div>
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" name="barang" value="<?php echo $data['nama'] ?>"></input>
                    <input type="hidden" class="form-control" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>"></input>
                </div>
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" name="jumlah" max="<?php echo $data['jumlah'];?>" value="1" min="1"></input>
                </div>
              
                <div class="form-group">
                    <label>Peminjam</label>
                      <select name="id_pegawai" class="form-control" required="" />
                        <?php 
                            $sql=mysqli_query($koneksi,"SELECT * FROM pegawai ");
                            while($tampil=mysqli_fetch_array($sql)){

                        ?>
                        <option value="<?php echo $tampil['id_pegawai'];?>"><?php echo $tampil['nama_pegawai'];?></option>
     `                   <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                        <input type="submit" name="simpan" class="btn btn-primary" value="simpan" >
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
        </div>
     

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>