<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. KIOS MALASNGODING',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.malasngoding.com email : malasngoding@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Inventaris",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal pinjam', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Jumlah', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Status', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Peminjaman', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($koneksi,"SELECT * FROM peminjaman p LEFT JOIN inventaris i ON p.id_inventaris=i.id_inventaris JOIN detail_pinjam d ON p.kode_peminjaman=d.kode_peminjaman WHERE status_peminjaman = 'pinjam' order by p.id_peminjaman asc");
while($data=mysqli_fetch_array($query)){
	 $id_pegawai = $data['id_pegawai'];
     $q_data_pegawai = mysqli_query($koneksi, "select * from pegawai where id_pegawai ='$id_pegawai'");
     $data_pegawai= mysqli_fetch_array($q_data_pegawai);
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $data['nama'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $data['tanggal_pinjam'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $data['jumlah'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $data['status_peminjaman'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $data_pegawai['nama_pegawai'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_inventaris.pdf","I");

?>


