 <!DOCTYPE html>
<?php
include "../koneksi.php";
?>
<html>
<head>
  <title>INVENSKANIC</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../data_table/assets/css/jquery.dataTables.css">
  <div class="panel panel-default">
  <style type="text/css">
  th{
    background-color: teal;
    color: white;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-default" >
<div class="panel-footer" style="background-color: teal;color: white;"> <img src="skanic.png" style="width : 3%">   INVENSKANIC
 <span style="float: right;color: white"><a href="logout.php"><i class="glyphicon glyphicon-log-out" style="color: white;font-size: larger;margin-top: 9px;"></i></a></span></div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
          </ul>
       
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_admin.php">Admin</a></li>
    <li><a href="d_operator.php">Operator</a></li>
    <li><a href="d_user.php">User</a></li>
  </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php"> Ruang</a></li>
    <li><a href="d_jenis.php"> Jenis</a></li>

  </ul>
          <li><a href="peminjaman_b.php"><span class="glyphicon glyphicon-resize-full"></span> Peminjam</a></li>
          <li><a href="pengembalian.php"><span class="glyphicon glyphicon-resize-small"></span> Pengembalian</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-print"></span> Laporan<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="laporan_barang.php">Data Barang</a></li>
    <li><a href="laporan_peminjaman2.php">Data peminjaman</a></li>
    <li><a href="laporan_pengembalian.php">Data Pengembalian</a></li>
          
          
        </li>
        </li>
       
      </ul>
          <li><a href="backup_database.php"><span class="glyphicon glyphicon-bookmark"></span> BackupDatabase</a></li>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php 
include 'tamdat_barang.php'; 
?>

<div class="container">
<br>
  <div  style="white-space: nowrap; font-size: 24px ">DATA Barang<span style="white-space: nowrap; font-size: 15px"> SMKN 1 Ciomas</span></div>
  <div class="kategori" style="padding-left: 20px;">
<?php   
    $sql=mysqli_query($koneksi,"SELECT * FROM inventaris GROUP BY sarana");
    while($data1=mysqli_fetch_array($sql)){
 
  echo"<a href='d_barang.php?sarana=$data1[sarana]' class='btn btn-primary' style='margin-left: 10px'>$data1[sarana]</a>";
  
    }
  ?>
</div>
<div class="kategori" style="float: right;">
  <a href='excel_brng.php' class='btn btn-primary'><span class="glyphicon glyphicon-print"></span> excel</a>
  <a href='pdf.php' class='btn btn-primary'><span class="glyphicon glyphicon-print"></span> pdf</a>
</div>
  
   <br>
  <div class="panel panel-default">
  <!-- Default panel contents -->
  
  <!-- Table -->
  <table class="table" id="example" style="margin-top: 10px">
    <thead>
      <tr class="">
      <th>no</th>
      <th>nama barang</th>
      <th>kondisi</th>
      <th>jumlah</th>
      <th>jenis</th>
      <th>tanggal register</th>
      <th>ruang</th>
      <th>kode inventaris</th>
    </tr>
    </thead>

    <tbody>
      <?php
      include"../koneksi.php";
      
      if(isset($_GET['sarana'])) {
      $pilih=mysqli_query($koneksi,"SELECT * from inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang nisa ON i.id_ruang=nisa.id_ruang WHERE i.sarana='$_GET[sarana]'");  
      }else{
      $pilih=mysqli_query($koneksi,"SELECT * from inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang nisa ON i.id_ruang=nisa.id_ruang ");
      }
      $no=1;
      while($data=mysqli_fetch_array($pilih)){
        ?>
        <style type="text/css">
        th{
          text-align: center;
        }
        td{
          text-align: center;
        }
        </style>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?=$data['nama'];?></td>
          <td><?=$data['kondisi']; ?></td>
        
          <td><?=$data['jumlah']; ?></td>
          <td><?=$data['nama_jenis']; ?></td>
          <td><?=$data['tanggal_register']; ?></td>
          <td><?=$data['nama_ruang']; ?></td>
          <td><?=$data['kode_inventaris']; ?></td>
          
          
        <div class="modal fade" id="myModal-check-<?php echo $data['id_inventaris'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">detail barang</h4>
      </div>
      <div class="modal-body">
        <img src="skanic.png" style="width : 25%" align="center">
        <br>nama barang: <?php echo $data['nama']; ?></br>
        <br>kondisi: <?php echo $data['kondisi']; ?></br>
        <br>keterangan: <?php echo $data['keterangan']; ?>
        <br>jumlah: <?php echo $data['jumlah']; ?></br>
        <br>jenis: <?php echo $data['nama_jenis']; ?></br>
        <br>tanggal register: <?php echo $data['tanggal_register']; ?></br>
        <br>ruang: <?php echo $data['nama_ruang']; ?></br>
        <br>kode inventaris: <?php echo $data['kode_inventaris']; ?></br>
        <br>sarana: <?php echo $data['sarana']; ?></br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><a href="hps_brng.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"> Hapus</a></button>
        <a href="peminjaman.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"><button type="button" class="btn btn-primary" name="simpan" >Pinjam</button></a>
      </div>
    </div>
  </div>
</div>          

        </tr>
        <!-- Modal -->
<div class="modal fade" id="myModal-edit-<?php echo $data['id_inventaris'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">masukan data</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="POST" action="edt_brng.php">
 
   <div class="form-group">
    <label for="nama">nama barang</label>
    <input type="hidden" class="form-control" name="id_inventaris" value="<?php echo $data['id_inventaris']; ?>">
    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $data['nama']; ?>">
  </div>
   <div class="form-group">
    <label for="kondisi">kondisi</label>
    <input type="text" class="form-control" id="kondisi" name="kondisi" placeholder="Masukan kondisi" value="<?php echo $data['kondisi'] ?>" >
  </div>
   <div class="form-group">
    <label for="keterangan">keterangan</label>
    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Masukan keterangan" value="<?php echo $data['keterangan'] ?>">
  </div>
   <div class="form-group">
    <label for="jumlah">jumlah</label>
    <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Masukan jumlah" value="<?php echo $data['jumlah'] ?>">
  </div>
   <div class="form-group">
    <label for="id_jenis">id jenis</label>
    <input type="text" class="form-control" id="id_jenis" name="id_jenis" placeholder="Masukan id" value="<?php echo $data['id_jenis'] ?>">
  </div>
  <div class="form-group">
    <label for="id_ruang">id ruang</label>
    <input type="text" class="form-control" id="id_ruang" name="id_ruang" placeholder="Masukan id" value="<?php echo $data['id_ruang'] ?>">
  </div>
  <div class="form-group">
    <label for="kode_inventaris">kode inventaris</label>
    <input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" placeholder="Masukan kode" value="<?php echo $data['kode_inventaris'] ?>">
  </div>
  <!--
<div class="form-group">
    <label for="sarana">sarana</label>
    <input type="text" class="form-control" id="sarana" name="sarana" placeholder="Masukan sarana">
  </div>
  <td><img src="<?php echo $data['gambar']; ?>" width="50" height="50"></td>
  -->
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="edit" >Submit</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>
      <?php
      $no++;
      }
      ?>
    </tbody>
  </table>
</div>

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>




















