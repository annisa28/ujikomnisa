<!DOCTYPE html>
<?php
include "../koneksi.php";
?>
<html>
<head>
	<title>INVENSKANIC</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link href="../css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../data_table/assets/css/jquery.dataTables.css">
	<div class="panel panel-default">
  <style type="text/css">
  th{
    background-color: teal;
    color: white;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-default" >
<div class="panel-footer" style="background-color: teal;color: white;"> <img src="skanic.png" style="width : 3%">   INVENSKANIC
 <span style="float: right;color: white"><a href="logout.php"><i class="glyphicon glyphicon-log-out" style="color: white;font-size: larger;margin-top: 9px;"></i></a></span></div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
          </ul>
       
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_admin.php">Admin</a></li>
    <li><a href="d_operator.php">Operator</a></li>
    <li><a href="d_user.php">User</a></li>
  </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php"> Ruang</a></li>
    <li><a href="d_jenis.php"> Jenis</a></li>

  </ul>
          <li><a href="peminjaman_b.php"><span class="glyphicon glyphicon-resize-full"></span> Peminjam</a></li>
          <li><a href="pengembalian.php"><span class="glyphicon glyphicon-resize-small"></span> Pengembalian</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-print"></span> Laporan<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="laporan_barang.php">Data Barang</a></li>
    <li><a href="laporan_peminjaman2.php">Data peminjaman</a></li>
    <li><a href="laporan_pengembalian.php">Data Pengembalian</a></li>
          
          
        </li>
        </li>
       
      </ul>
          <li><a href="backup_database.php"><span class="glyphicon glyphicon-bookmark"></span> BackupDatabase</a></li>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php 
include 'tambdat_operator.php'; 
?>

<div class="container">
	<div  style="white-space: nowrap; font-size: 24px ">DATA OPERATOR<span style="white-space: nowrap; font-size: 15px"> SMKN 1 Ciomas</span></div>
  <a href="" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="float: right;">+ Tambah Data</a><br>
   <br>
  <div class="panel panel-default">
  <!-- Default panel contents -->
  
  <!-- Table -->
  <table class="table" id="example">
    <thead>
      <tr class="">
      <th>no</th>
      <th>id</th>
      <th>username</th>
      <th>password</th>
      <th>nama petugas</th>
      <th>level</th>
      <th>aksi</th>
    </tr>
    </thead>

    <tbody>
      <?php
      $no=1;
      $pilih=mysqli_query($koneksi,"SELECT * from petugas WHERE id_level='2'");
      while($data=mysqli_fetch_array($pilih)){
        ?>
        <style type="text/css">
        th{
          text-align: center;
        }
        td{
          text-align: center;
        }
        </style>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?=$data['id_petugas'];?></td>
          <td><?=$data['username']; ?></td>
          <td><?=$data['password']; ?></td>
          <td><?=$data['nama_petugas']; ?></td>
          <td><?=$data['id_level']; ?></td>
          <td>
          <a href="#" class="glyphicon glyphicon-edit"  data-toggle="modal" data-target="#myModal-edit-<?php echo $data['id_petugas'] ?>" style=" font-size: 20px"></a>
          <a style="font-size: 20px" class="glyphicon glyphicon-trash" href="hps_oprtr.php?id=<?php echo $data['id_petugas']; ?>"></a>         
        </td>
          

        </tr>
        <!-- Modal -->
<div class="modal fade" id="myModal-edit-<?php echo $data['id_petugas'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">masukan data</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="POST" action="edt_oprtr.php">
 
   <div class="form-group">
    <label for="username">username</label>
    <input type="hidden" class="form-control" id="id" name="id_petugas" value="<?php echo $data['id_petugas']; ?>">
    <input type="text" class="form-control" id="username" name="username" placeholder="Masukan nama" value="<?php echo $data['username'] ?>">
  </div>
   <div class="form-group">
    <label for="password">password</label>
    <input type="text" class="form-control" id="password" name="password" placeholder="Masukan password" value="<?php echo $data['password'] ?>" >
  </div>
   <div class="form-group">
    <label for="nama_petugas">nama petugas</label>
    <input type="text" class="form-control" id="nama_petugas" name="nama_petugas" placeholder="Masukan nama" value="<?php echo $data['nama_petugas'] ?>">
  </div>
<!--    <div class="form-group">
    <label for="level">level</label>
    <input type="text" class="form-control" id="level" name="level" placeholder="Masukan level">
  </div> -->
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="edit" >Submit</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>
      <?php
      $no++;
      }
      ?>
    </tbody>
  </table>
</div>

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>