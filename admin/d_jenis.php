  <!DOCTYPE html>
<?php
include "../koneksi.php";
?>
<html>
<head>
	<title>IVENSKANIC</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link href="../css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../data_table/assets/css/jquery.dataTables.css">
	<div class="panel panel-default">
  <style type="text/css">
  th{
    background-color: teal;
    color: white;
  }
  </style>
</head>
<body>

<nav class="navbar navbar-default" >
<div class="panel-footer" style="background-color: teal;color: white;"> <img src="skanic.png" style="width : 3%">   INVENSKANIC
 <span style="float: right;color: white"><a href="logout.php"><i class="glyphicon glyphicon-log-out" style="color: white;font-size: larger;margin-top: 9px;"></i></a></span></div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
          </ul>
       
      
<ul class="nav navbar-nav navbar-right">
        <li><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Pengguna<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_admin.php">Admin</a></li>
    <li><a href="d_operator.php">Operator</a></li>
    <li><a href="d_user.php">User</a></li>
  </ul>
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> Inventaris<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="d_barang.php">Barang</a></li>
    <li><a href="d_ruang.php"> Ruang</a></li>
    <li><a href="d_jenis.php"> Jenis</a></li>

  </ul>
          <li><a href="peminjaman_b.php"><span class="glyphicon glyphicon-resize-full"></span> Peminjam</a></li>
          <li><a href="pengembalian.php"><span class="glyphicon glyphicon-resize-small"></span> Pengembalian</a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-print"></span> Laporan<span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="laporan_barang.php">Data Barang</a></li>
    <li><a href="laporan_peminjaman2.php">Data peminjaman</a></li>
    <li><a href="laporan_pengembalian.php">Data Pengembalian</a></li>
          
          
        </li>
        </li>
       
      </ul>
          <li><a href="backup_database.php"><span class="glyphicon glyphicon-bookmark"></span> BackupDatabase</a></li>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php 
include 'tamdat_jenis.php'; 
?>

<div class="container">
	<div  style="white-space: nowrap; font-size: 24px ">DATA JENIS<span style="white-space: nowrap; font-size: 15px"> SMKN 1 Ciomas</span></div>
   <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="float: right;">+ Tambah Data</a><br>
   <br>
  <div class="panel panel-default">
  <!-- Default panel contents -->
  
  <!-- Table -->
  <table class="table" id="example">
    <thead>
      <tr class="">
      <th>no</th>
      <th>nama jenis</th>
      <th>kode jenis</th>
      <th>keterangan</th>
      <th>aksi</th>
    </tr>
    </thead>

    <tbody>
      <?php
      $no=1;
      $pilih=mysqli_query($koneksi,"SELECT * from jenis");
      while($data=mysqli_fetch_array($pilih)){
        ?>
        <style type="text/css">
        th{
          text-align: center;
        }
        td{
          text-align: center;
        }
        </style>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?=$data['nama_jenis'];?></td>
          <td><?=$data['kode_jenis']; ?></td>
          <td><?=$data['keterangan']; ?></td>
          <td>
          <a href="#" class="glyphicon glyphicon-edit"  data-toggle="modal" data-target="#myModal-edit-<?php echo $data['id_jenis'] ?>" style=" font-size: 20px"></a>
          <a style="font-size: 20px" class="glyphicon glyphicon-trash" href="hps_jenis.php?id=<?php echo $data['id_jenis']; ?>"></a>         
        </td>
          

        </tr>
        <!-- Modal -->
<div class="modal fade" id="myModal-edit-<?php echo $data['id_jenis'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">masukan data</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="POST" action="edt_jenis.php">
 <input type="hidden" class="form-control" name="id_jenis" value="<?php echo $data['id_jenis']; ?>">
   <div class="form-group">
    <label for="nama_jenis">nama jenis</label>
    <input type="text" class="form-control" name="nama_jenis" value="<?php echo $data['nama_jenis']; ?>">
  </div>
   <div class="form-group">
    <label for="kode_jenis">kode_jenis</label>
    <input type="number" class="form-control" id="kode_jenis" name="kode_jenis" placeholder="Masukan ruang" value="<?php echo $data['kode_jenis'] ?>" >
  </div>
   <div class="form-group">
    <label for="keterangan">keterangan</label>
    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Masukan nama" value="<?php echo $data['keterangan'] ?>">
  </div>
<!--    <div class="form-group">
    <label for="level">level</label>
    <input type="text" class="form-control" id="level" name="level" placeholder="Masukan level">
  </div> -->
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="edit" >Submit</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>
      <?php
      $no++;
      }
      ?>
    </tbody>
  </table>
</div>

</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../data_table/assets/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
    $('#example').DataTable();
  });
</script>
</body>
</html>